CREATE TABLE links (
    id SERIAL,
    url VARCHAR,
    short VARCHAR
);